/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.mycompany.eva3.dao;

import cl.mycompany.eva3.dao.exceptions.NonexistentEntityException;
import cl.mycompany.eva3.dao.exceptions.PreexistingEntityException;
import cl.mycompany.eva3.entity.Entradas;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Gonzalo
 */
public class entradasJpaController implements Serializable {

    public entradasJpaController() {
       
    }
   EntityManagerFactory emf = Persistence.createEntityManagerFactory("eventos_pu");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Entradas entradas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entradas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEntradas(entradas.getFolioEntrada()) != null) {
                throw new PreexistingEntityException("Entradas " + entradas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Entradas entradas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            entradas = em.merge(entradas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = entradas.getFolioEntrada();
                if (findEntradas(id) == null) {
                    throw new NonexistentEntityException("The entradas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Entradas entradas;
            try {
                entradas = em.getReference(Entradas.class, id);
                entradas.getFolioEntrada();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The entradas with id " + id + " no longer exists.", enfe);
            }
            em.remove(entradas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Entradas> findEntradasEntities() {
        return findEntradasEntities(true, -1, -1);
    }

    public List<Entradas> findEntradasEntities(int maxResults, int firstResult) {
        return findEntradasEntities(false, maxResults, firstResult);
    }

    private List<Entradas> findEntradasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Entradas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Entradas findEntradas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Entradas.class, id);
        } finally {
            em.close();
        }
    }

    public int getEntradasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Entradas> rt = cq.from(Entradas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
