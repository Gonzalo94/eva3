/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.mycompany.eva3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gonzalo
 */
@Entity
@Table(name = "entradas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entradas.findAll", query = "SELECT e FROM Entradas e"),
    @NamedQuery(name = "Entradas.findByNombreEvento", query = "SELECT e FROM Entradas e WHERE e.nombreEvento = :nombreEvento"),
    @NamedQuery(name = "Entradas.findByPreventaEntrada", query = "SELECT e FROM Entradas e WHERE e.preventaEntrada = :preventaEntrada"),
    @NamedQuery(name = "Entradas.findByPrecioEntrada", query = "SELECT e FROM Entradas e WHERE e.precioEntrada = :precioEntrada"),
    @NamedQuery(name = "Entradas.findByFechaEntrada", query = "SELECT e FROM Entradas e WHERE e.fechaEntrada = :fechaEntrada"),
    @NamedQuery(name = "Entradas.findByFolioEntrada", query = "SELECT e FROM Entradas e WHERE e.folioEntrada = :folioEntrada")})
public class Entradas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "nombre_evento")
    private String nombreEvento;
    @Size(max = 2147483647)
    @Column(name = "preventa_entrada")
    private String preventaEntrada;
    @Size(max = 2147483647)
    @Column(name = "precio_entrada")
    private String precioEntrada;
    @Size(max = 2147483647)
    @Column(name = "fecha_entrada")
    private String fechaEntrada;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "folio_entrada")
    private String folioEntrada;

    public Entradas() {
    }

    public Entradas(String folioEntrada) {
        this.folioEntrada = folioEntrada;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getPreventaEntrada() {
        return preventaEntrada;
    }

    public void setPreventaEntrada(String preventaEntrada) {
        this.preventaEntrada = preventaEntrada;
    }

    public String getPrecioEntrada() {
        return precioEntrada;
    }

    public void setPrecioEntrada(String precioEntrada) {
        this.precioEntrada = precioEntrada;
    }

    public String getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(String fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public String getFolioEntrada() {
        return folioEntrada;
    }

    public void setFolioEntrada(String folioEntrada) {
        this.folioEntrada = folioEntrada;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (folioEntrada != null ? folioEntrada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entradas)) {
            return false;
        }
        Entradas other = (Entradas) object;
        if ((this.folioEntrada == null && other.folioEntrada != null) || (this.folioEntrada != null && !this.folioEntrada.equals(other.folioEntrada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.mycompany.eva3.entity.Entradas[ folioEntrada=" + folioEntrada + " ]";
    }
    
}
