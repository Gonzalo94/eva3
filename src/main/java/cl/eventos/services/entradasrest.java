/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.eventos.services;

import cl.mycompany.eva3.dao.entradasJpaController;
import cl.mycompany.eva3.dao.exceptions.NonexistentEntityException;
import cl.mycompany.eva3.entity.Entradas;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Gonzalo
 */
@Path("entradas")
public class entradasrest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response lista() {

        entradasJpaController dao = new entradasJpaController();
        List<Entradas> entradas = dao.findEntradasEntities();
        return Response.ok(200).entity(entradas).build();

    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response Crear (Entradas entrada) {
         entradasJpaController dao = new entradasJpaController();
        try {
            dao.create(entrada);
        } catch (Exception ex) {
            Logger.getLogger(entradasrest.class.getName()).log(Level.SEVERE, null, ex);
        }
         
          return Response.ok(200).entity(entrada).build();
        
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response Actualizar (Entradas entrada) {
         entradasJpaController dao = new entradasJpaController();
        try {
            dao.edit(entrada);
        } catch (Exception ex) {
            Logger.getLogger(entradasrest.class.getName()).log(Level.SEVERE, null, ex);
        }
          return Response.ok(200).entity(entrada).build();
        
    }
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar (@PathParam("ideliminar") String ideliminar) {
         entradasJpaController dao = new entradasJpaController();
        
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(entradasrest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
          return Response.ok("elemento eliminado").build();
    }
    
    @GET
    @Path("/{idconsulta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultaid (@PathParam("idconsulta") String idconsulta) {
        
        entradasJpaController dao = new entradasJpaController();
        
        Entradas entrada=dao.findEntradas(idconsulta);
        
        return Response.ok(200).entity(entrada).build();
        
    }

}
