<%-- 
    Document   : index
    Created on : 04-jul-2021, 22:56:45
    Author     : Gonzalo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>endpoint api entradas para eventos</h1>
        <h1>lista de entradas a eventos get: https://solemneapi3.herokuapp.com/api/entradas</h1>
        <h1>lista de entradas a eventos get-consulta: https://solemneapi3.herokuapp.com/api/entradas/{idconsulta}</h1>
        <h1>lista de entradas a eventos post: https://solemneapi3.herokuapp.com/api/entradas</h1>
        <h1>lista de entradas a eventos put: https://solemneapi3.herokuapp.com/api/entradas</h1>
        <h1>lista de entradas a eventos delete: https://solemneapi3.herokuapp.com/api/entradas/{ideliminar}</h1>
        
        
    </body>
</html>
